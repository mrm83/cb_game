<?
require_once '/srv/www/game.cowbeans.com/public_html/vendor/autoload.php';
define('COMMON_LIB', dirname(__FILE__));
define('COMMON_LIB_CACHE', COMMON_LIB . '/cache');

function common_autoloader($classname){
  if(file_exists($file = COMMON_LIB . '/models/' . str_replace('_', '/', $classname) . '.php')){
    require_once $file;
  }
  
  $db = COMMON_LIB . '/config.php';
  require_once $db;
}

spl_autoload_register('common_autoloader');

?>
