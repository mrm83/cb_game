<?
/*
  $db = Db::connection(db);
  $query = "blah";
  $db->exec($query); //inserts
  $db->query($query);
  $row = $result->fetch(PDO::FETCH_ASSOC);
*/

class Db extends PDO {
  private static $connectionPool;

  public static function connection($dsn){
    if(!isset(self::$connectionPool[$dsn])){
      $pattern = "/^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/";
      if(preg_match($pattern,$dsn,$matches)){
        $protocol = $matches[1];
        $username = $matches[4];
        $password = $matches[5];
        $host = $matches[6];
        $port = $matches[7];
        $db = $matches[11];

        self::$connectionPool[$dsn] = new Db("$protocol:dbname=$db;host=$host",$username,$password);
      } else {
        throw new Exception("Could not parse DSN : $dsn");
      }
    }
    return self::$connectionPool[$dsn];
  }

  public function query($statement){
    $ret = parent::query($statement);
    if(!$ret){
      $error_info = $this->errorInfo();
      $trace = debug_backtrace();
      //log
    }
    return $ret;
  }
}
?>
