var jew = {
  screens: {},
  settings: {
    rows: 8,
    cols: 8,
    baseScore: 100,
    numJewTypes: 7,
    jewelSize: 40,
    controls: {
      KEY_UP: "moveUp",
      KEY_LEFT: "moveLeft",
      KEY_DOWN: "moveDown",
      KEY_RIGHT: "moveRight",
      KEY_ENTER: "selectJewel",
      KEY_SPACE: "selectJewel",
      CLICK: "selectJewel",
      TOUCH: "selectJewel"
    }
  },
  images: {}
};

jew.game = (function() {
  var logging = true;
  
  function showScreen(screenId) {
    var activeScreen = $('#gameContainer .screen.active');
    var screen = $('#' + screenId);
    
    if(jew.screens[screenId] == undefined) {
      log('oops ' + screenId + ' does not exist');
      return;
    }
    
    if (activeScreen) {
      activeScreen.removeClass('active');
    }
    screen.addClass('active');
    jew.screens[screenId].run();
  }
      
  function createBackground() {
    var canvas = document.createElement('canvas');
    var ctx = canvas.getContext('2d');
    var background = $("#gameContainer .background");
    var width = background.width();
    var height = background.height();
    
    var gradient;
    var i;
    
    canvas.width = width;
    canvas.height = height;
    
    ctx.scale(width, height);
    gradient = ctx.createRadialGradient(0.25, 0.15, 0.5, 0.25, 0.15, 1);
    gradient.addColorStop(0, "rgb(55, 65, 50)");
    gradient.addColorStop(1, "rgb(0, 0, 0)");
    ctx.fillStyle = gradient;
    ctx.fillRect(0, 0, 1, 1);

    ctx.strokeStyle = "rgba(255, 255, 255, .02)";
    ctx.strokeStyle = "rgba(0, 0, 0, .2)";
    ctx.lineWidth = .008;
    ctx.beginPath();
    for (i = 0; i < 2; i+=.02) {
      ctx.moveTo(i, 0);
      ctx.lineTo(i - 1, 1);
    }
    ctx.stroke();
    background.append(canvas);
  }
  
  function setup() {
    createBackground();
  }
  
  function log(text) {
    if(logging) {
      console.log(text);
    }
  }
  
  return {
    showScreen : showScreen,
    setup: setup
  };
})();

$(function() {
  jew.game.setup();
  jew.game.showScreen('splashScreen');
  var image = new Image();
  image.src = "../public_html/static/images/jewels40.png";
  jew.images["static/images/jewels40.png"] = image;
});
