jew.input = (function() {
  var settings = jew.settings;
  var inputHandlers;
  
  function initialize() {
    inputHandlers = {};
    var board = $('#gameScreen .gameBoard');
    board.on('mousedown', function(e) {
        handleClick(e, "CLICK", e);
    });
  }
  
  function bind(action, handler) {
    if (!inputHandlers[action]) {
      inputHandlers[action] = [];
    }
    inputHandlers[action].push(handler);
  }
  
  function trigger(action) {
    var handlers = inputHandlers[action];
    var args = Array.prototype.slice.call(arguments, 1);

    if (handlers) {
      for (var i = 0; i < handlers.length; i++) {
        handlers[i].apply(null, args);
      }
    }
  }
  
  function handleClick(event, control, click) {
    var action = settings.controls[control];
    if(!action) {
      return;
    }
    
    var board = $('#gameScreen .gameBoard');
    var offset = board.offset();
    var relX, relY;
    var jewelX, jewelY;
    
    relX = click.clientX - offset.left;
    relY = click.clientY - offset.top;
    
    jewelX = Math.floor(relX / board.width() * settings.cols);
    jewelY = Math.floor(relY / board.height() * settings.rows);
    
    trigger(action, jewelX, jewelY);
    event.preventDefault();
  }
  
  return {
    initialize: initialize,
    bind: bind
  };
  
})();
