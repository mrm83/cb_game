jew.screens["splashScreen"] = (function() {
  var game = jew.game;
  var firstRun = true;
  
  function setup() {
    $("#splashScreen").on("click", function() {
      game.showScreen('mainMenu');
    });
  }
  
  function run() {
      if (firstRun) {
        setup();
        firstRun = false;
      }
  }
  
  return {
    run: run
  };
})();
