jew.display = (function() {
    var canvas;
    var ctx;
    var jewelSize;
    var firstRun = true;
    var cursor;
    var jewels;
    
    function setup() {
      var boardElement = $("#gameScreen .gameBoard");
      
      cols = jew.settings.cols;
      rows = jew.settings.rows;
      jewelSize = jew.settings.jewelSize;
      
      canvas = document.createElement("canvas");
      ctx = canvas.getContext("2d");
      canvas.className += " " + "board";
      canvas.width = cols * jewelSize;
      canvas.height = rows * jewelSize;
      
      boardElement.append(createBackground());
      boardElement.append(canvas);
    }
    
    function moveJewels(movedJewels, callback) {
      var n = movedJewels.length;
      var mover;
      var i;
      
      for (i = 0; i < n; i++) {
        mover = movedJewels[i];
        clearJewel(mover.fromX, mover.fromY);
      }
      for (i = 0; i < n; i++) {
        mover = movedJewels[i];
        drawJewel(mover.type, mover.toX, mover.toY);
      }
      callback();
    }
    
    function removeJewels(removedJewels, callback) {
      var n = removedJewels.length;
      for (var i = 0; i < n; i++) {
        clearJewel(removedJewels[i].x, removedJewels[i].y);
      }
      callback();
    }
    
    function drawJewel(type, x, y) {
      var image = jew.images["static/images/jewels" + jewelSize + ".png"];
      ctx.drawImage(image, type * jewelSize, 0 , jewelSize, jewelSize, x * jewelSize, y * jewelSize, jewelSize, jewelSize);
    }
    
    function clearJewel(x, y) {
      ctx.clearRect(x * jewelSize, y * jewelSize, jewelSize, jewelSize);
    }
    
    function redraw(newJewels, callback) {
      var x;
      var y;
      jewels = newJewels;
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      for (x = 0; x < cols; x++) {
        for (y = 0; y < rows; y++) {
          drawJewel(jewels[x][y], x, y);
        }
      }
      callback();
      renderCursor();
    }
    
    function renderCursor() {
      if (!cursor) {
        return;
      }
      var x = cursor.x;
      var y = cursor.y;
      
      clearCursor();
      if (cursor.selected) {
        ctx.save();
        ctx.globalCompositeOperation = "lighter";
        ctx.globalAlpha = 0.8;
        drawJewel(jewels[x][y], x , y);
        ctx.restore();
      }
      ctx.save();
      ctx.lineWidth = 0.05 * jewelSize;
      ctx.strokeStyle = "rgba(250, 250, 150, 0.8)";
      ctx.strokeRect((x + 0.05) * jewelSize, (y + 0.05) * jewelSize, 0.9 * jewelSize, 0.9 * jewelSize);
      ctx.restore();
    }
    
    function createBackground() {
      var background = document.createElement("canvas");
      var bgctx = background.getContext("2d");
      background.className += " " + "background";
      background.width = cols * jewelSize;
      background.height = rows * jewelSize;
      
      bgctx.fillStyle = "rgba(225, 235, 255, 0.15)";
      for (var x = 0; x < cols; x++) {
        for (var y = 0; y < cols; y++) {
          if ((x + y) % 2) {
            bgctx.fillRect(x * jewelSize, y * jewelSize, jewelSize, jewelSize);
          }
        }
      }
      return background;
    }
    
    function clearCursor() {
      if (cursor) {
        var x = cursor.x;
        var y = cursor.y;
        clearJewel(x, y);
        drawJewel(jewels[x][y], x, y);
      }
    }
    
    function setCursor(x, y, selected) {
      clearCursor();
      if (arguments.length > 0) {
        cursor = {
          x: x,
          y: y,
          selected: selected
        };      
      } else {
        cursor = null;
      }
      renderCursor();
    }
    
    function initialize(callback) {
      if (firstRun) {
        setup();
        firstRun = false;
      }
      callback();
    }
    
    return {
      initialize: initialize,
      redraw: redraw,
      setCursor: setCursor,
      moveJewels: moveJewels,
      removeJewels: removeJewels
    }
})();
