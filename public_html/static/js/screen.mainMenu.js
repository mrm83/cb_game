jew.screens["mainMenu"] = (function() {
  var game = jew.game;
  var firstRun = true;
  
  function setup() {
    $("#mainMenu ul.menu").on('click', function() {
      var action = $(this).parent().find('button').attr('name');
      game.showScreen(action);
    });
  }
  
  function run() {
    if (firstRun) {
      setup();
      firstRun = false;
    }
  }
  
  return {
    run: run
  };
})();
